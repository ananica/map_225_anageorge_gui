package com.example.social_network_gui.service;

import com.example.social_network_gui.domain.*;
import com.example.social_network_gui.repository.Repository;
import com.example.social_network_gui.validators.ValidationException;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;


/**
 * repoUser - User Repository
 * repoFriendships - Friendships repository
 */
public class Service {
    private Repository<Long, User> repoUser;
    private Repository<Tuple<Long, Long>, Friendship> repoFriends;
    private Repository<Tuple<Long,Long>, FriendshipStatus> repoStatus;
    private Repository<Long, Message> repoMessage;

    public Service(Repository<Long, User> RepoUser, Repository<Tuple<Long, Long>, Friendship> RepoFriends, Repository<Tuple<Long,Long>, FriendshipStatus> repoStatus1, Repository<Long, Message> repoMessage) {
        repoFriends = RepoFriends;
        repoUser = RepoUser;
        repoStatus = repoStatus1;
        this.repoMessage = repoMessage;
    }

    /**
     * function that adds an user's friends
     */
    private void friends_list() {
        for (Friendship fr : repoFriends.findAll()) {
            User u1 = repoUser.findOne(fr.getId().getId1());
            User u2 = repoUser.findOne(fr.getId().getId2());
            u1.addFriend(u2);
            u2.addFriend(u1);
        }
    }

    /**
     * function that saves an user
     * @param firstName String
     * @param lastName String
     */
    public void save(String firstName, String lastName) {
        User user = new User(firstName, lastName);
        long id = 0L;
        for (User ur : repoUser.findAll()) {
            if (ur.getId() > id)
                id = ur.getId();

        }
        id++;
        user.setId(id);
        User save = repoUser.save(user);
        if (save != null)
            throw new ValidationException("id already used");


    }

    /**
     * function that updates an user
     * @param id - Long
     * @param firstName - String
     * @param lastName-String
     */
    public void update(Long id, String firstName, String lastName) {

            User user = new User(firstName, lastName);
            if(findOneUser(id) == null)
                throw new ValidationException("non existent id");
            user.setFriendsList(findOneUser(id).getFriendsList());
            user.setId(id);
            for (User ur : repoUser.findAll()) {
                ArrayList<User> copyFriends = new ArrayList<>(ur.getFriendsList());
                for (User ur1 : copyFriends) {

                    if (Objects.equals(ur1.getId(), id)) {

                        ur.deleteFriend(ur1);
                        ur.addFriend(user);
                    }
                }
            }
            User save = repoUser.update(user);
            if (save != null)
                throw new ValidationException("non existent id");


    }

    /**
     * function that deletes an user
     * @param id - Long
     * @return User
     */
    public Entity delete(Long id) {

            Entity deleted = repoUser.delete(id);
           /// User deletedUser = repoUser.delete(id);

            if (deleted == null)
                throw new ValidationException("invalid id");

            ArrayList<Friendship> copy = new ArrayList<>((Collection<? extends Friendship>) repoFriends.findAll());
            for (Friendship fr : copy)
            {
                if (Objects.equals(fr.getId().getId1(), id) || Objects.equals(fr.getId().getId2(), id))
                    repoFriends.delete(fr.getId());
            }
            for (User ur : repoUser.findAll())
            {
                ArrayList<User> copyFriends=new ArrayList<>(ur.getFriendsList());
                for (User ur1 : copyFriends) {
                    if (ur1 == deleted)
                    {
                        ur.deleteFriend(ur1);
                    }
                }
            }
            return deleted;
    }

    /**
     * function that deletes a friend
     * @param id1 = Long
     * @param id2 = Long
     */
    public void addFriend(Long id1, Long id2) {
        FriendshipStatus fr = new FriendshipStatus();
        Tuple t = new Tuple(id1, id2);
        fr.setId(t);
        fr.setStatus("pending");
        if (repoUser.findOne(id1) == null)
            throw new ValidationException("first id does not exist");
        if (repoUser.findOne(id2) == null)
            throw new ValidationException("second id does not exist");
       Tuple invers = new Tuple(id2,id1);
       if(repoStatus.findOne(invers) != null && (repoStatus.findOne(invers).getStatus().compareTo("approved") == 0 || repoStatus.findOne(invers).getStatus().compareTo("pending") == 0))
            throw new ValidationException("Request already exists");
       if(repoStatus.findOne(fr.getId()) != null) {
           if (repoStatus.findOne(fr.getId()).getStatus().compareTo("declined") == 0)
               repoStatus.update(fr);
           else throw new ValidationException("Request already exists");
       }
       else repoStatus.save(fr);

        /*Entity save = repoFriends.save(fr);
        if (save != null)
            throw new ValidationException("ids already used");
        User u1 = repoUser.findOne(id1);
        User u2 = repoUser.findOne(id2);
        u1.addFriend(u2);
        u2.addFriend(u1);*/


    }

    public void responseRequest(Long id1, Long id2, String status) {
        if(status.compareTo("approved") != 0 && status.compareTo("declined") != 0)
            throw new ValidationException("Invalid response");
        FriendshipStatus fr = new FriendshipStatus();
        Tuple t = new Tuple(id1, id2);
        fr.setId(t);
        fr.setStatus(status);
        if(repoStatus.findOne(fr.getId()) == null)
            throw new ValidationException("Request does not exist");
        else if(repoStatus.findOne(fr.getId()).getStatus().compareTo("pending") != 0)
            throw new ValidationException("This request already has a response");
        repoStatus.update(fr);
        if(fr.getStatus().compareTo("approved") == 0) {
            Friendship friendship = new Friendship();
            friendship.setId(new Tuple(id1,id2));
            friendship.setDate(LocalDateTime.now());
            repoFriends.save(friendship);
        }
    }

    public Iterable<FriendshipStatus> findAllRequest() { return repoStatus.findAll();}

    /**
     * function that deletes a friend
     * @param id1 - Long
     * @param id2 - Long
     */
    public void deleteFriend(Long id1, Long id2) {

        Tuple t = new Tuple(id1, id2);
        if (repoUser.findOne(id1) == null)
            throw new ValidationException("first id does not exist");
        if (repoUser.findOne(id2) == null)
            throw new ValidationException("second id does not exist");

        Entity save = repoFriends.delete(t);
        if (save == null)
            throw new ValidationException("invalid ids");
        repoStatus.delete(t);
        repoUser.findOne(id1).deleteFriend(repoUser.findOne(id2));
        repoUser.findOne(id2).deleteFriend(repoUser.findOne(id1));

    }

    /**
     * @return the maximum id, Long
     */
    public Long get_size() {
        Long maxim = 0L;
        for (User ur : repoUser.findAll())
            if (ur.getId() > maxim)
                maxim = ur.getId();
        return maxim;
    }

    /**
     * @return users list
     */
    public Iterable<User> printUsers() {
        return repoUser.findAll();
    }

    /**
     *
     * @return friendships list
     */
    public Iterable<Friendship> printFriends() {
        return repoFriends.findAll();
    }

    /**
     *
     * @param id - Long
     * @return user with given id
     */
    public User findOneUser(Long id)
    {
        return repoUser.findOne(id);
    }
    public Iterable<User> allUsers()
    {
        Iterable<User> allusers = repoUser.findAll();
        Iterable<Friendship> allfr = repoFriends.findAll();
        for (User u : allusers) {
            for (Friendship f : allfr) {
                if (f.getId().getId1() == u.getId())
                    u.addFriend(findOneUser(f.getId().getId2()));
                if (f.getId().getId2() == u.getId())
                    u.addFriend(findOneUser(f.getId().getId1()));
            }
        }
        return allusers;
    }

    public void saveMessage(Long fromId, List<Long> toIds, String message)
    {
        User from=repoUser.findOne(fromId);
        List<User> to=new ArrayList<>();
        toIds.forEach(x->to.add(repoUser.findOne(x)));
        //System.out.println(to);
        Message mess=new Message( from,  to,  message,null);
        mess.setDate(LocalDateTime.now());
        long id = 0L;
        for (Message ms:  repoMessage.findAll()) {
            if (ms.getId() > id)
                id = ms.getId();

        }
        id++;

        mess.setId(id);

        Message save =repoMessage.save(mess);
        if (save != null)
            throw new ValidationException("id already used");


    }
    public void saveReply(Long fromId, String message,Long reply)
    {
        User from=repoUser.findOne(fromId);
        List<User> to=new ArrayList<>();
        to =findTo(reply,fromId);
        checkMessageReply(reply,fromId);
        Message replyMess=repoMessage.findOne(reply);
        Message mess=new Message( from,to,message,replyMess);
        mess.setDate(LocalDateTime.now());
        long id = 0L;
        for (Message ms:  repoMessage.findAll()) {
            if (ms.getId() > id)
                id = ms.getId();

        }
        id++;

        mess.setId(id);

        Message save =repoMessage.save(mess);
        if (save != null)
            throw new ValidationException("id already used");

    }
    public Message findOne(Long id)
    {
        if(repoMessage.findOne(id) != null)
            return repoMessage.findOne(id);
        else
            throw new ValidationException("message id invalid");
    }
    public List<User> findTo(Long idMessageOld,Long idMessageNew)
    {
        Message messOld=findOne(idMessageOld);
        List<User> listTo=messOld.getToReply(repoUser.findOne(idMessageNew));
        User oldFrom=messOld.getFrom();
        listTo.add(oldFrom);
        return listTo;
    }
    public User checkMessageReply(Long idMessageOld,Long idUserNew)
    {
        Message mess=findOne(idMessageOld);
        User userFromNew=repoUser.findOne(idUserNew);
        if(Objects.equals(userFromNew.getId(), mess.getFrom().getId()))
            throw new ValidationException("can not reply to yourself") ;

        for(User ur:mess.getTo())
        {
            if(Objects.equals(userFromNew.getId(), ur.getId()))
                return userFromNew;}
        throw new ValidationException("can not reply to a message from a conversation you don not belong") ;
    }
    public List<Message> showPrivateChat(Long id1,Long id2)
    {
        List<Message> conversation=new ArrayList<>();
        User user1=repoUser.findOne(id1);
        User user2=repoUser.findOne(id2);
        List<Message> messages=new ArrayList<>();
        repoMessage.findAll().forEach(messages ::add);
        List<Message> sortedMessages=messages
                .stream()
                .sorted(Comparator.comparing(Entity::getId))
                .collect(Collectors.toList());
        for(Message mess:sortedMessages)
        {

            List<Long> ids = new ArrayList<>();
            mess.getTo().forEach(x->ids.add(x.getId()));
            //&& mess.getTo().size()==1 - de scos daca nu vrem sa excludem conversatiile de pe grup
            if((Objects.equals(id1, mess.getFrom().getId()) && ids.contains(user2.getId()) && mess.getTo().size()==1 )
                    || (Objects.equals(id2, mess.getFrom().getId()) && ids.contains(user1.getId()))&& mess.getTo().size()==1  )
                conversation.add(mess);

        }
        return conversation;
    }

    public User userexist(long id) {
        for (User ur : repoUser.findAll())
            if (Objects.equals(ur.getId(), id))
                return repoUser.findOne(id);
        throw new ValidationException(" id invalid");
    }


    public Map<User,LocalDateTime> friends_month(String id1, String month1) {
        Long id = Long.parseLong(id1);
        int month = Integer.parseInt(month1);
        if(repoUser.findOne(id) == null)
            throw new ValidationException("there is no user with the given id");
        Map<User,LocalDateTime> friends = new HashMap();
        List<Friendship> friendships = new ArrayList();
        repoFriends.findAll().forEach(friendships::add);
        friendships.stream().filter(x->x.getId().getId1().equals(id)).filter(y->y.getDate().getMonthValue() == month).forEach(z->friends.put(repoUser.findOne(z.getId().getId2()),z.getDate()));
        friendships.stream().filter(x->x.getId().getId2().equals(id)).filter(y->y.getDate().getMonthValue() == month).forEach(z->friends.put(repoUser.findOne(z.getId().getId1()),z.getDate()));
        return friends;
    }

    public Map<User,LocalDateTime> afis_data(Long id)
    {
        User verif = repoUser.findOne(id);
        if(verif == null)
            throw new ValidationException("non-existent id");
        HashMap<User,LocalDateTime> allusers = new HashMap<>();
        List<Friendship> friendships = new ArrayList<>();
        repoFriends.findAll().forEach(friendships::add);

        friendships.stream().filter(x -> x.getId().getId1().equals(id)).forEach(y ->allusers.put(findOneUser(y.getId().getId2()),y.getDate()));
        friendships.stream().filter(x -> x.getId().getId2().equals(id)).forEach(y ->allusers.put(findOneUser(y.getId().getId1()),y.getDate()));
        return allusers;


    }
}
