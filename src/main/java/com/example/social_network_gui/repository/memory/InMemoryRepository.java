package com.example.social_network_gui.repository.memory;

import com.example.social_network_gui.domain.Entity;
import com.example.social_network_gui.repository.Repository;
import com.example.social_network_gui.validators.Validator;

import java.util.HashMap;
import java.util.Map;

/**
 * class that implements Repository
 * @param <ID>
 * @param <E>
 */
public class InMemoryRepository<ID, E extends Entity<ID>> implements Repository<ID,E> {

    private Validator<E> validator;
    Map<ID,E> entities;

    public InMemoryRepository(Validator<E> validator) {
        this.validator = validator;
        entities=new HashMap<ID,E>();
    }

    /**
     * function that overrides findOne method
     * @param id -the id of the entity to be returned
     *           id must not be null
     * @return E
     */
    @Override
    public E findOne(ID id){
        if (id==null)
            throw new IllegalArgumentException("id must be not null");
        return entities.get(id);
    }

    /**
     * function that overrides findAll method
     * @return E - iterable
     */
    @Override
    public Iterable<E> findAll() {
        return entities.values();
    }

    /**
     * function that overrides save method
     * @param entity
     *         entity must be not null
     * @return E / null
     */
    @Override
    public E save(E entity) {
        if (entity==null)
            throw new IllegalArgumentException("entity must be not null");
        validator.validate(entity);
        if(entities.get(entity.getId()) != null) {
            return entity;
        }
        else entities.put(entity.getId(),entity);
        return null;
    }

    /**
     * function that overrides delete method
     * @param id
     *      id must be not null
     * @return E / null
     */
    @Override
    public E delete(ID id) {
        if(entities.get(id)==null)
            return null;
        E entity=entities.get(id);
        entities.remove(id);
        return entity;
    }

    /**
     * function that overrides update method
     * @param entity
     *          entity must not be null
     * @return E /null
     */
    @Override
    public E update(E entity) {

        if(entity == null)
            throw new IllegalArgumentException("entity must be not null!");
        validator.validate(entity);

        if(entities.get(entity.getId()) != null) {
            entities.put(entity.getId(),entity);
            return null;
        }
        return entity;

    }


}