package com.example.social_network_gui.repository.file;

import com.example.social_network_gui.domain.User;
import com.example.social_network_gui.validators.Validator;

import java.util.List;

/***
 * class that extends AbstractFileRepository
 * ID = Long
 * E = User
 */
public class UserInFileRepository extends AbstractFileRepository<Long, User> {
        public UserInFileRepository(String fileName, Validator<User> validator) {
        super(fileName, validator);
        }

        /**
         * function that overrides extractEntity method
         * @param attributes = List of strings
         * @return user
         */
        @Override
        protected User extractEntity(List<String> attributes) {
        User user = new User(attributes.get(1), attributes.get(2));
        user.setId(Long.parseLong(attributes.get(0)));
        return user;
        }

        /**
         * function that overrides createEntity method
         * @param entity = user
         * @return String
         */
        @Override
        protected String createEntityAsString(User entity) {
        return entity.getId() + ";" + entity.getFirstName() + ";" + entity.getLastName();
}
}
