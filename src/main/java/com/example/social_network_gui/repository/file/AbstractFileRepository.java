package com.example.social_network_gui.repository.file;

import com.example.social_network_gui.domain.Entity;
import com.example.social_network_gui.repository.memory.InMemoryRepository;
import com.example.social_network_gui.validators.Validator;

import java.io.*;
import java.util.Arrays;
import java.util.List;

/**
 * class that extends InMemoryRepository
 * @param <ID>
 * @param <E>
 */
public abstract class AbstractFileRepository<ID, E extends Entity<ID>> extends InMemoryRepository<ID, E> {
    String fileName;

    public AbstractFileRepository(String fileName, Validator<E> validator) {
        super(validator);
        this.fileName = fileName;
        loadData();
    }

    /**
     * function that loads data from a file
     * it catches exceptions
     */
    private void loadData() {
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            String line;
            while ((line = br.readLine()) != null) {
                List<String> attributes = Arrays.asList(line.split(";"));
                E entity = extractEntity(attributes);
                super.save(entity);
                System.out.println(line);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * function that extracts a type E entity
     * @param attributes
     * @return type E entity
     */
    protected abstract E extractEntity(List<String> attributes);

    /**
     * function that creates a type E entity
     * @param entity
     * @return
     */
    protected abstract String createEntityAsString(E entity);

    /**
     * function that writes data to a file
     * it catches exceptions
     * @param entity = type E
     */
    protected void writeToFile(E entity) {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(fileName, true))) {
            bw.write(createEntityAsString(entity));
            bw.newLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * function that writes all the data to a file
     * it catches exception
     */
    protected void writeToFile() {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(fileName, false))) {
            super.findAll().forEach(entity -> {
                try {
                    bw.write(createEntityAsString(entity));
                    bw.newLine();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * function that overrides save method
     * @param entity = type E
     * @return entity/ null
     */
    @Override
    public E save(E entity) {
        if (super.save(entity) == null) {
            writeToFile(entity);
            return null;
        } else
            return entity;
    }

    /**
     * function that overrides update method
     *
     * @param entity = E type
     * @return entity/null
     */
    @Override
    public E update(E entity) {
        E updated_entity = super.update(entity);
        if (updated_entity == null) {
            writeToFile();
            return null;
        }
        return entity;
    }

    /**
     * function that overrides delete method
     * @param id = ID type
     * @return entitu/null
     */
    @Override
    public E delete(ID id) {
        E deleted_entity = super.delete(id);
        if (deleted_entity == null) {

            return null;
        }
        writeToFile();
        return deleted_entity;
    }
}

