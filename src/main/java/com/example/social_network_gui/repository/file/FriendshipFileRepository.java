package com.example.social_network_gui.repository.file;

import com.example.social_network_gui.domain.Friendship;
import com.example.social_network_gui.domain.Tuple;
import com.example.social_network_gui.validators.Validator;

import java.time.LocalDateTime;
import java.util.List;

/**
 * class that extends AbstractFileRepository
 * ID = tuple<Long,Long>
 * E = friendship
 */
public class FriendshipFileRepository extends AbstractFileRepository<Tuple<Long,Long>, Friendship>{


    public FriendshipFileRepository(String fileName, Validator<Friendship> validator) {
        super(fileName, validator);
    }

    /**
     * fumction that overides extractEntity method
     * @param attributes = Strings list
     * @return friendship
     */
    @Override
    protected Friendship extractEntity(List<String> attributes) {
        long id1=Long.parseLong(attributes.get(0));
        long id2=Long.parseLong(attributes.get(1));
        LocalDateTime date=LocalDateTime.parse(attributes.get(2));
        Friendship friendship=new Friendship();

        Tuple t=new Tuple(id1,id2);
        friendship.setId(t);
        friendship.setDate(date);
        return  friendship;
    }

    /**
     * function that overrides createEntity method
     * @param entity = friendship
     * @return String
     */
    @Override
    protected String createEntityAsString(Friendship entity) {
        return entity.getId().getId1()+";"+entity.getId().getId2()+";"+entity.getDate();
    }
}
