package com.example.social_network_gui.repository.datebase;

import com.example.social_network_gui.domain.Message;
import com.example.social_network_gui. domain.User;
import com.example.social_network_gui.repository.Repository;
import com.example.social_network_gui.validators.Validator;

import java.sql.*;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;
import java.util.stream.Collectors;

public class MessageDbRepository implements Repository<Long, Message> {
    private String url;
    private String username;
    private String password;
    private Validator<Message> validator;
    public MessageDbRepository(String url, String username, String password, Validator<Message> validator) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
    }
    @Override
    public Message findOne(Long aLong) {
        if(aLong == null)
            throw new IllegalArgumentException("id must not be null");
        String sql = "Select * from messages where messages.id =?";
        Message message;
        try(Connection connection = DriverManager.getConnection(url,username,password);
            PreparedStatement statement = connection.prepareStatement(sql))
        {
            statement.setInt(1, Math.toIntExact(aLong));
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next())
            {
                Long id = resultSet.getLong("id");
                LocalDateTime date = LocalDateTime.ofInstant(resultSet.getTimestamp("datem").toInstant(), ZoneOffset.ofHours(0));
                Long fromId = resultSet.getLong("fromm");
                User from = findOneUser(fromId);
                String to1 = resultSet.getString("tom");
                List<String> toId =new ArrayList<String>(Arrays.asList(to1.split(" ")));
                toId.remove(0);
                List<User> to = new ArrayList<>();
                List<Long> ids = toId.stream()
                        .map(Long::parseLong)
                        .collect(Collectors.toList());
                ids.forEach(x -> to.add(findOneUser(x)));
                String msg = resultSet.getString("messagem");
                Long idReply = resultSet.getLong("replym");
                Message reply = this.findOne(idReply);
                message = new Message(from,to,msg);
                message.setId(id);
                message.setDate(date);
                message.setReply(reply);
                return  message;

            }
        }
        catch (SQLException ex)
        {
            ex.printStackTrace();
        }
        return null;
    }
    private User findOneUser(Long id)
    {
        if(id == null)
            throw new IllegalArgumentException("id must not be null");
        String sql = "select * from users where users.id = ?";
        User user;
        try(Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement(sql))
        {
            statement.setInt(1, Math.toIntExact(id));
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                String firstName = resultSet.getString("first_name");
                String lastName = resultSet.getString("last_name");

                user = new User(firstName, lastName);
                user.setId(id);
                user.setFriendsList(this.findFriend(id));
                return user;
            }
        }catch (SQLException ex)
        {
            ex.printStackTrace();
        }
        return null;
    }

    private List<User> findFriend(Long id) {
        List<User> users = new ArrayList<>();
        try(Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement("select id, first_name, last_name, date\n" +
                    "from users u inner join friendship f on u.id = f.id1 or u.id=f.id2\n" +
                    "where (f.id1= ? or f.id2 = ? )and u.id!= ?"))
        {
            statement.setLong(1, id);
            statement.setLong(2, id);
            statement.setLong(3,id);
            try(ResultSet resultSet = statement.executeQuery())
            {
                while(resultSet.next())
                {
                    Long id_new = resultSet.getLong("id");
                    String first_name = resultSet.getString("first_name");
                    String lasrt_name = resultSet.getString("last_name");
                    User user = new User(first_name,lasrt_name);
                    user.setId(id_new);
                    users.add(user);
                }
                return users;
            }
        }catch(SQLException ex)
        {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public Iterable<Message> findAll() {
        Set<Message> messages = new HashSet<>();
        Message message = null;
        try(Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement("SELECT * from messages order by datem");
            ResultSet resultSet = statement.executeQuery())
        {
            while(resultSet.next())
            {
                Long id = resultSet.getLong("id");
                LocalDateTime date = LocalDateTime.ofInstant(resultSet.getTimestamp("datem").toInstant(), ZoneOffset.ofHours(0));
                Long fromId = resultSet.getLong("fromm");
                User from = findOneUser(fromId);
                String to1 = resultSet.getString("tom");
                List<String> toId = new ArrayList<String>(Arrays.asList(to1.split(" ")));
                toId.remove(0);
                List<User> to= new ArrayList<>();
                List<Long> ids = toId.stream()
                        .map(Long::parseLong)
                        .collect(Collectors.toList());
                ids.forEach(x -> to.add(findOneUser(x)));
                String msg = resultSet.getString("messagem");
                Long idReply = resultSet.getLong("replym");
                Message reply = this.findOne(idReply);
                message = new Message(from,to,msg);
                message.setId(id);
                message.setDate(date);
                message.setReply(reply);
                messages.add(message);
            }
            return messages;
        }catch (SQLException ex)
        {
            ex.printStackTrace();
        }
        return messages;
    }

    @Override
    public Message save(Message entity) {
        if(entity == null)
            throw new IllegalArgumentException("entity must not be null");
        validator.validate(entity);
        Message message = null;
        String sql = "insert into messages (fromm,tom,messagem,replym,datem) values(?,?,?,?,'"+entity.getDate()+"')";
        try(Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement ps = connection.prepareStatement(sql))
        {
            message = this.findOne(entity.getId());
            if(message != null)
                return message;
            ps.setLong(1,entity.getFrom().getId());
            String string = String.valueOf(entity.getTo()
                    .stream()
                    .map(x -> x.getId().toString())
                    .reduce("",(x,y) -> x+" "+y));
            ps.setString(2,string);
            ps.setString(3,entity.getMessage());
            if(entity.getReply() != null)
                ps.setLong(4,entity.getReply().getId());
            else
                ps.setLong(4,-1L);
            ps.executeUpdate();
        }catch (SQLException ex)
        {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public Message delete(Long aLong) {
        return null;
    }

    @Override
    public Message update(Message entity) {
        return null;
    }
}
