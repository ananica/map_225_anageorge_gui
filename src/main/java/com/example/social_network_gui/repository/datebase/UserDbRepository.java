package com.example.social_network_gui.repository.datebase;

import com.example.social_network_gui.domain.User;
import com.example.social_network_gui.repository.Repository;
import com.example.social_network_gui.validators.Validator;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class UserDbRepository implements Repository<Long, User> {
    private String url;
    private String username;
    private String password;
    private Validator<User> validator;
    public UserDbRepository(String url, String username, String password, Validator<User> validator) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
    }
    @Override
    public User findOne(Long id_user) {
        if (id_user==null)
            throw new IllegalArgumentException("id must be not null");
        String sql = "select * from users where id = ?";
        try (Connection connection = DriverManager.getConnection(url,username,password);
             PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setLong(1,id_user);
            ResultSet resultSet = ps.executeQuery();
            while(resultSet.next())
            {
                Long id = resultSet.getLong("id");
                String first_name = resultSet.getString("first_name");
                String last_name = resultSet.getString("last_name");
                User user = new User(first_name,last_name);
                user.setId(id);
                return user;

            }

        }catch (SQLException ex)
        {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public Iterable<User> findAll() {
        List<User> users = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from users");
             ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                Long id = resultSet.getLong("id");
                String first_name = resultSet.getString("first_name");
                String last_name = resultSet.getString("last_name");
                User utilizator = new User(first_name, last_name);
                utilizator.setId(id);
                users.add(utilizator);
            }
            return users;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return users;
    }
    @Override
    public User save(User entity) {
        if (entity == null)
            throw new IllegalArgumentException("entity must not be null");
        validator.validate(entity);
        String sql = "insert into users (first_name, last_name ) values (?, ?)";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setString(1, entity.getFirstName());
            ps.setString(2, entity.getLastName());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
    @Override
    public User delete(Long id_user) {
        if (id_user==null)
            throw new IllegalArgumentException("id must be not null");
        String sql = "delete from users where id = ?";
        int c = 0;
        try(Connection connection = DriverManager.getConnection(url,username,password);
        PreparedStatement ps = connection.prepareStatement(sql)){
            ps.setLong(1,id_user);
            User removed = findOne(id_user);
            c = ps.executeUpdate();
            if (c > 0)
                return removed;
        }catch (SQLException ex)
        {
            ex.printStackTrace();
        }
        return null;
    }
    @Override
    public User update(User entity) {
        if(entity == null)
            throw new IllegalArgumentException("entity mast not be null");
        validator.validate(entity);
        String sql = "update users set first_name=?,last_name=? where id = ?";
        int c = 0;
        try(Connection connection = DriverManager.getConnection(url,username,password);
        PreparedStatement ps = connection.prepareStatement(sql))
        {
            ps.setString(1,entity.getFirstName());
            ps.setString(2,entity.getLastName());
            ps.setLong(3,entity.getId());
            c = ps.executeUpdate();
        }catch (SQLException ex)
        {
            ex.printStackTrace();
        }
        if(c > 0)
            return null;
        return entity;
    }
}