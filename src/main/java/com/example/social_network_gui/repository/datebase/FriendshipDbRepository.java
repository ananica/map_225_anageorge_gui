package com.example.social_network_gui.repository.datebase;

import com.example.social_network_gui.domain.Friendship;
import com.example.social_network_gui.domain.Tuple;
import com.example.social_network_gui.repository.Repository;
import com.example.social_network_gui.validators.Validator;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class FriendshipDbRepository implements Repository<Tuple<Long,Long>, Friendship> {
    private String url;
    private String username;
    private String password;
    private Validator<Friendship> validator;
    public FriendshipDbRepository(String url, String username, String password, Validator<Friendship> validator) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
    }
    @Override
    public Friendship findOne(Tuple<Long, Long> longLongTuple) {
        if (longLongTuple==null)
            throw new IllegalArgumentException("id must be not null");
        String sql = "select * from friendship where id1 = ? and id2=?";
        try (Connection connection = DriverManager.getConnection(url,username,password);
             PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setLong(1,longLongTuple.getId1());
            ps.setLong(2,longLongTuple.getId2());
            ps.executeQuery();
            ResultSet resultSet = ps.executeQuery();
            while(resultSet.next())
            {
                Long id1 = resultSet.getLong("id1");
                Long id2 = resultSet.getLong("id2");
                String date_string = resultSet.getString("date");
                LocalDateTime date_ldt = LocalDateTime.parse(date_string);
                Friendship friendship = new Friendship();
                Tuple t = new Tuple(id1,id2);
                friendship.setId(t);
                friendship.setDate(date_ldt);
                return friendship;
            }
        }catch (SQLException ex)
        {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public Iterable<Friendship> findAll() {
        List<Friendship> friendships = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from friendship");
             ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                Long id1 = resultSet.getLong("id1");
                Long id2 = resultSet.getLong("id2");
                String date_string = resultSet.getString("date");
                LocalDateTime date_ldt = LocalDateTime.parse(date_string);
                Friendship friendship = new Friendship();
                Tuple t = new Tuple(id1,id2);
                friendship.setId(t);
                friendship.setDate(date_ldt);
                friendships.add(friendship);
            }
            return friendships;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return friendships;
    }

    @Override
    public Friendship save(Friendship entity) {
        if (entity== null)
            throw new IllegalArgumentException("entity must not be null");
        validator.validate(entity);
        String sql = "insert into friendship (id1, id2,date ) values (?, ?,?)";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setLong(1, entity.getId().getId1());
            ps.setLong(2, entity.getId().getId2());
            ps.setString(3,entity.getDate().toString());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Friendship delete(Tuple<Long, Long> longLongTuple) {
        if (longLongTuple==null)
            throw new IllegalArgumentException("id must be not null");
        String sql = "delete from friendship where id1 = ? and id2 = ?";
        int c = 0;
        try(Connection connection = DriverManager.getConnection(url,username,password);
            PreparedStatement ps = connection.prepareStatement(sql)){
            ps.setLong(1,longLongTuple.getId1());
            ps.setLong(2,longLongTuple.getId2());
            Friendship removed = findOne(longLongTuple);
            c = ps.executeUpdate();
            if ( c > 0)
                return removed;
        }catch (SQLException ex)
        {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public Friendship update(Friendship entity) {
        return null;
    }
}
