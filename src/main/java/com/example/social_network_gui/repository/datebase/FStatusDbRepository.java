package com.example.social_network_gui.repository.datebase;

import com.example.social_network_gui.domain.FriendshipStatus;
import com.example.social_network_gui.domain.Tuple;
import com.example.social_network_gui.repository.Repository;
import com.example.social_network_gui.validators.Validator;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class FStatusDbRepository implements Repository<Tuple<Long,Long>, FriendshipStatus> {
    private String url;
    private String username;
    private String password;
    private Validator<FriendshipStatus> validator;
    public FStatusDbRepository(String url, String username, String password, Validator<FriendshipStatus> validator) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
    }
    @Override
    public FriendshipStatus findOne(Tuple<Long, Long> longLongTuple) {
        if (longLongTuple==null)
            throw new IllegalArgumentException("id must be not null");
        String sql = "select * from friendship_status where id1 = ? and id2=?";
        try (Connection connection = DriverManager.getConnection(url,username,password);
             PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setLong(1,longLongTuple.getId1());
            ps.setLong(2,longLongTuple.getId2());
            ps.executeQuery();
            ResultSet resultSet = ps.executeQuery();
            while(resultSet.next())
            {
                Long id1 = resultSet.getLong("id1");
                Long id2 = resultSet.getLong("id2");
                String status = resultSet.getString("status");
                FriendshipStatus friendship = new FriendshipStatus();
                Tuple t = new Tuple(id1,id2);
                friendship.setId(t);
                friendship.setStatus(status);
                return friendship;
            }
        }catch (SQLException ex)
        {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public Iterable<FriendshipStatus> findAll() {
        List<FriendshipStatus> friendships = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from friendship_status");
             ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                Long id1 = resultSet.getLong("id1");
                Long id2 = resultSet.getLong("id2");
                String status = resultSet.getString("status");
                FriendshipStatus friendship = new FriendshipStatus();
                Tuple t = new Tuple(id1,id2);
                friendship.setId(t);
                friendship.setStatus(status);
                friendships.add(friendship);
            }
            return friendships;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return friendships;
    }

    @Override
    public FriendshipStatus save(FriendshipStatus entity) {
        if (entity== null)
            throw new IllegalArgumentException("entity must not be null");
        validator.validate(entity);
        String sql = "insert into friendship_status (id1, id2,status) values (?, ?,?)";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setLong(1, entity.getId().getId1());
            ps.setLong(2, entity.getId().getId2());
            ps.setString(3,entity.getStatus());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public FriendshipStatus delete(Tuple<Long, Long> longLongTuple) {
        if (longLongTuple==null)
            throw new IllegalArgumentException("id must be not null");
        String sql = "delete from friendship_status where id1 = ? and id2 = ?";
        int c = 0;
        try(Connection connection = DriverManager.getConnection(url,username,password);
            PreparedStatement ps = connection.prepareStatement(sql)){
            ps.setLong(1,longLongTuple.getId1());
            ps.setLong(2,longLongTuple.getId2());
            FriendshipStatus removed = findOne(longLongTuple);
            c = ps.executeUpdate();
            if ( c > 0)
                return removed;
        }catch (SQLException ex)
        {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public FriendshipStatus update(FriendshipStatus entity) {
        if (entity== null)
            throw new IllegalArgumentException("entity must not be null");
        validator.validate(entity);
        if(this.findOne(entity.getId()) == null)
            return entity;
        String sql = "update friendship_status set status = ? where id1 = ? and id2 = ?";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setString(1, entity.getStatus());
            ps.setLong(2, entity.getId().getId1());
            ps.setLong(3,entity.getId().getId2());
            ps.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
