package com.example.social_network_gui.validators;

import com.example.social_network_gui.domain.FriendshipStatus;

import java.util.Objects;

public class ValidationFriendshipStatus implements  Validator<FriendshipStatus>{


    @Override
    public void validate(FriendshipStatus entity) throws ValidationException {
        if (Objects.equals(entity.getId().getId1(), entity.getId().getId2()))
            throw new ValidationException("the ids must be different");
    }
}
