package com.example.social_network_gui.validators;

public interface Validator<T> {
    void validate(T entity) throws ValidationException;

}