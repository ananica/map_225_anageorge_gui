package com.example.social_network_gui.validators;

import com.example.social_network_gui.domain.User;

public class ValidationUser implements Validator<User> {
    @Override
    public void validate(User entity) throws ValidationException {
        String first=entity.getFirstName();
        String last= entity.getLastName();
        Long id= entity.getId();
        if(!first.matches("^.[a-z]{0,24}$"))
            throw new ValidationException("the first name must contain only small letters[25 max], except the first one ");
        if(!first.matches("^[A-Z].*"))
            throw new ValidationException2("the first name must start with a big letter");
        if(!last.matches("^.[a-z]{0,24}$"))
            throw new ValidationException("the last name must contain only small letters[25 max], except the first one");
        if(!last.matches("^[A-Z].*"))
           throw new ValidationException2("the last name must start with a big letter");


    }

}

