package com.example.social_network_gui.validators;

import com.example.social_network_gui.domain.Friendship;

import java.util.Objects;

public class ValidationFriendship implements  Validator<Friendship>{


    @Override
    public void validate(Friendship entity) throws ValidationException {
        if(Objects.equals(entity.getId().getId1(), entity.getId().getId2()))
            throw new ValidationException("the ids must be different");

    }

}
