package com.example.social_network_gui.domain;

import java.time.LocalDateTime;

public class Friendship extends Entity<Tuple<Long,Long>> {

    LocalDateTime date;

    public Friendship(){}

    /**
     *
     * @return the date when the friendship was created
     */
    public LocalDateTime getDate() {
        return date;
    }

    /**
     * set the date time when the friendship was created
     * @param Date-LocalDateTime
     */
    public void setDate(LocalDateTime Date){date=Date;}

    /**
     * overrride toString method
     * @return the ids of 2 user that are in a friendship
     */
    @Override
    public String toString() {
        return "Friendship{" +getId().getId1()+" "+getId().getId2()+" "+
                "date=" + date +
                '}';
    }
}
