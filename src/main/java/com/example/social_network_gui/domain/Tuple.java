package com.example.social_network_gui.domain;


import java.util.Objects;


/**
 * Define a Tuple o generic type entities
 * @param <E1> - tuple first entity type
 * @param <E2> - tuple second entity type
 */
public class Tuple<E1, E2> {
    private E1 e1;
    private E2 e2;

    public Tuple(E1 e1, E2 e2) {
        this.e1 = e1;
        this.e2 = e2;
    }

    /**
     *
     * @return teh first value of the tuple
     */
    public E1 getId1() {
        return e1;
    }

    /**
     * funtion that sets the first value of the tuple
     * @param e1 = E1 type
     */
    public void setId1(E1 e1) {
        this.e1 = e1;
    }

    /**
     *
     * @return the second value of the tuple
     */
    public E2 getId2() {
        return e2;
    }

    /**
     * funvtion that sets the second value of the tuple
     * @param e2 = E2 type
     */
    public void setId2(E2 e2) {
        this.e2 = e2;
    }

    /**
     * function that overrides toString method
     * @return the values of the tuple
     */
    @Override
    public String toString() {
        return "" + e1 + "," + e2;

    }

    /**
     * function that overrides equals method
     * @param obj = Object type
     * @return boolean
     */
    @Override
    public boolean equals(Object obj) {
        return this.e1.equals(((Tuple) obj).e1) && this.e2.equals(((Tuple) obj).e2);
    }

    /**
     * function that overrides hashCode method
     * @return integer
     */
    @Override
    public int hashCode() {
        return Objects.hash(e1, e2);
    }
}
