package com.example.social_network_gui.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * class that extends Entity
 * firstName = String
 * lastName = String
 * FriendsList = Users list
 */
public class User extends Entity<Long>{
    private String firstName, lastName;
    private List<User> FriendsList = new ArrayList<>();

    public User(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    /**
     *
     * @return the user's firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * function that sets user's firstName
     * @param firstName = string
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     *
     * @return user's lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * function that sets user's lastName
     * @param lastName = String
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     *
     * @return user's friends list
     */
    public List<User> getFriendsList() {
        return FriendsList;
    }

    /**
     * functions that sets user's friends list
     * @param friendsList
     */
    public void setFriendsList(List<User> friendsList) {
        this.FriendsList = friendsList;
    }

    /**
     * function that adds an user to a friendsList
     * @param friend = User
     */
    public void addFriend(User friend) {
        this.FriendsList.add(friend);
    }

    /**
     * function that deletes an user from a friendsList
     * @param friend
     */
    public void deleteFriend(User friend) {
        this.FriendsList.remove(friend);
    }

    /**
     * function that overrides toStrong method
     * @return firstName,lastName, FriendsList
     */

    @Override
    public String toString() {
        String s;
        s = "User{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", friends=";
        for (int i = 0; i < FriendsList.size(); i++)
            s = s + "|"+FriendsList.get(i).getFirstName() + " " + FriendsList.get(i).getLastName();
        s = s + '}';
        return s;
    }

    /**
     * function that overrides equals method
     * @param o = Object
     * @return boolean
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(firstName, user.firstName) && Objects.equals(lastName, user.lastName) && Objects.equals(FriendsList, user.FriendsList);
    }

    /**
     * function that overrides hashCode method
     * @return integer
     */
    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, FriendsList);
    }
}

