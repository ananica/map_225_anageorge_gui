package com.example.social_network_gui.domain;

public class FriendshipStatus extends Entity<Tuple<Long,Long>>{
    private String status;

    public FriendshipStatus() {}

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "FriendshipStatus{" +
                id.getId1() + " " +
                id.getId2() +
                ", status='" + status + '\'' +
                '}';
    }
}
