package com.example.social_network_gui.domain;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Network = class for community operations
 * mat = matrix with integer values
 * size = the size of the matrix, integer number
 * int = a set with keys (Long values)
 */
public class Network {
    private Integer[][] mat;
    private Integer size;
    private Set<Long> ind;

    public Network(int size) {
        this.ind = new HashSet<>();
        this.mat = new Integer[size][size];
        this.size = size;
        for (int i = 0; i < size; i++)
            for (int j = 0; j < size; j++)
                this.mat[i][j] = 0;
    }

    /**
     * function that add a friendship
     * @param list  = friendships list
     */
    public void addFriendships(Iterable<Friendship> list) {
        list.forEach(f ->
                this.mat[(int) (f.getId().getId1() - 1)][(int) (f.getId().getId2()- 1)] = 1);
        list.forEach(f ->
                this.mat[(int) (f.getId().getId2() - 1)][(int) (f.getId().getId1() - 1)] = 1);

    }

    /**
     * function that adds users
     * @param list = users list
     */
    public void addUsers(Iterable<User> list) {

        list.forEach(x -> ind.add(x.getId()-1));

    }

    /**
     * DFS algorithm
     * @param v = integer
     * @param visited = boolean array
     */
    private void DFSUtils(int v, boolean[] visited) {
        visited[v] = true;
        for (int i = 0; i < size; i++)
            if (mat[v][i] == 1 && !visited[i])
                DFSUtils(i, visited);
    }

    /**
     * function that count the number of communities
     * @return integer
     */
    public int connectedComponents() {
        int nr = 0;
        boolean[] visited = new boolean[size];
        for (int i = 0; i < size; i++) {

            if (!visited[i] && ind.contains(Long.valueOf(i))) {
                DFSUtils(i, visited);
                nr++;
            }
        }
        return nr;
    }

    /**
     * function that find the most sociable community
     * @return
     */
    public List<Integer> biggestComponent() {
        List<Integer> list = new ArrayList<>();
        int max = 0;
        connectedComponents();
        for (int i = 0; i < size; i++) {
            int aux = 0;
            for (int j = 0; j < size; j++)
                if (mat[i][j] == 1)
                    aux++;
            if (aux > max) {
                max = aux;
                list.clear();
                list.add(i+1);
                for (int j = 0; j < size; j++)
                    if (mat[i][j] == 1)
                        list.add(j+1);
            }
        }
        return list;
    }

}
